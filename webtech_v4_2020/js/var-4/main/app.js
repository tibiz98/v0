function removeOrderItem(orderInfo, position){
    if(!Array.isArray(orderInfo.items)){
        throw new Error('Items should be an array')
    }
    else{
        for(let i=0;i<orderInfo.items.length;i++){
            if(!orderInfo.items[i].price||!orderInfo.items[i].quantity){
                throw new Error('Malformed item')
            }
        }
        if(position<0 || position>orderInfo.items.length-1 ){
            throw new Error('Invalid position')
        }
        else{
            orderInfo.items.splice(position,1)
            orderInfo.total=orderInfo.items.reduce((total,el)=>total+el.price*el.quantity,0)
            return orderInfo
        }
    }
}

const app = {
    removeOrderItem
};

module.exports = app;