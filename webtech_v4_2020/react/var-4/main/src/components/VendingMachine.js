import React, { Component } from 'react'
import Product from './Product'
import ProductStore from '../stores/ProductStore'

class VendingMachine extends Component {
    constructor() {
        super()
        this.state = {
            products: [],
            tokens: 0
        }

        this.addToken = () => {
            let tok=this.state.tokens
            tok++
            this.setState({
                tokens:tok
            })
        }

        this.buyProduct = (price) => {
           if(price<=this.state.tokens){
               let prevTokens=this.state.tokens
               prevTokens-=price
               this.setState({
                   tokens: prevTokens
               })
           }
        }
    }

    componentDidMount(){
		this.store = new ProductStore()
		this.setState({
			products : this.store.getAll()
		})
		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				companies : this.store.getAll()
			})			
		})
	}
	
	buyProduct=()=>{
	    
	}

    render() {
        return (
            <div>
                {this.state.products.map((el, index) => <Product key={index} name={el.name} price={el.price} onBuy={this.buyProduct}  />)}
                <div>Tokens: {this.state.tokens}</div>
                <input type="button" value="add token" onClick={this.addToken} />
            </div>
        )
    }
}

export default VendingMachine