import React, { Component } from 'react'

class Product extends Component {
    render(){
        return (
            <div>
                `${this.props.name} ${this.props.price}`
                <button value="buy" onClick={()=>{
                    this.props.onBuy(this.props.price)
                }}/>
            </div>
        )
    }
}

export default Product