const express = require('express')
const jso=require('./public/profile.json')
const app = express()

app.use('/',express.static('public'))

app.get('/profile.json',(req,res)=>{
    res.status(200).json(jso)
})

module.exports = app